<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\tbl_chucvu;
use App\tbl_chucvu_permission;
use App\tbl_permissions;


class AdminLoginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd($permission);
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->quyen >= 0) {
                return $next($request);
            } else
                return redirect('login');
        } else
            return redirect('login');
    }
}
