
@extends('layout.index')
@section('content')

<div class="page-wrapper">
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">LỊCH SỬ CHẤM CÔNG</h2>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- end validation form -->
                <!-- ============================================================== -->
                @if(isset($chamcong->id_tangca) || isset($tangca))
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class=" card-header text-bold h5 text-danger">Hôm nay có tăng ca mà bạn đã đăng ký</div>
                        <div class="card-body">
                            <a href="{{url('private/chamcong/tangca')}}" class="btn btn-outline-dark"> Điểm Danh Tăng Ca </a>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">LỊCH SỬ CHẤM CÔNG</h5>
                        <div class="card-body">
                            <table class="table table-striped table-bordered" id="data-tables-check">
                                <thead>
                                   <tr align="center">
                                        <th>Ngày</th>
                                        <th>Giờ Vào</th>
                                        <th>Giờ ra</th>
                                        <th>Thời Gian Làm</th>
                                        <th>Tăng ca</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($lichsu as $ls)
                                    <tr class="even gradeC" align="center">
                                        <td>
                                        @if(date('d/m',strtotime($ls->check_in)) == date('d/m'))
                                        Hôm nay
                                        @else
                                            {{date('d/m',strtotime($ls->check_in))}}
                                        @endif
                                        </td>
                                        <td>{{date('H:i:s',strtotime($ls->check_in))}}</td>
                                        <td>
                                        @if(isset($ls->thoi_gian_lam))
                                            {{date('H:i:s',($ls->thoi_gian_lam * 3600) + strtotime($ls->check_in))}} <!-- cong thuc bi sai -->
                                        @else
                                            Đang Làm Việc
                                        @endif
                                        </td>
                                        <td>
                                        @if(isset($ls->thoi_gian_lam))
                                            {{round($ls->thoi_gian_lam,1)}} Tiếng
                                        @endif
                                        </td>
                                        <td>
                                        @if(isset($ls->id_tangca))
                                            Có <a href="{{url('private/chamcong/tangca/chitiet/'.$ls->id_tangca)}}"> (xem chi tiết)</a>
                                        @else
                                            Không có
                                        @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                                     <!-- /.row -->
                        </div>
                        <div class="h5 alert alert-info"> Trong tháng {{date('m')}} bạn đã làm được {{round($bangluong->so_gio_lam_viec,1)}} giờ </div>
                    </div>
                </div>
        </div>
    </div>
</div>

@endsection

